import './App.css';
import { Link, Route, Routes } from "react-router-dom";
import { Switch } from 'react-router'

import  KeysComponent  from './components/keyscomponents';
import SignatureComponent from './components/signaturecomponents'
import TransactionComponent from './components/transactioncomponents'
import BlockchainComponent from './components/blockchaincomponents';

const links = [
  { href: '/keys', label: 'Keys' },
  { href: '/signature', label: 'Signature' },
  { href: '/transaction', label: 'Transaction' },
  { href: '/blockchain', label: 'Blockchain' },
]

function App() {

  return (
    <>
    <header className="sticky top-0 z-50 bg-[#343a40]">
      <div className="ml-auto mr-auto flex h-[55px] w-11/12 items-center">
        <nav  className='justify-between w-full'>
          <ul className="flex items-center justify-between">
            <div className="font-normal text-white text-xl">Blockchain Demo: Public / Private Keys & Signing</div>
            <div className='flex flex-row space-x-5 text-lg'>
              {
                links.map(({ href, label }) => (
                <li key={`${href}${label}`} className='text-[rgba(255,255,255,.5)] hover:text-[rgba(255,255,255,.75)]'>
                  <Link to={href}>
                    {label}
                  </Link>
                </li>
              ))
              }
            </div>
          </ul>
        </nav>
      </div>
    </header>
    <Routes>
        <Route path="/keys" element={<KeysComponent />} />
        <Route path="/signature" element={<SignatureComponent />}></Route>
        <Route path="/transaction" element={<TransactionComponent />}></Route>
        <Route path="/blockchain" element={<BlockchainComponent />}></Route>
    </Routes>
    </>
  );
}

export default App;
