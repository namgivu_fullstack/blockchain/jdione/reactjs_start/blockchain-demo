import React from 'react'
import { Card, CardHeader, Heading, CardBody, Text, CardFooter, Button, InputGroup, InputRightAddon, Input  } from '@chakra-ui/react'

const KeysComponent = () => {
  return (
    <div className="">
        <div className="pt-8 flex justify-center">
            <Card width={1200} height={300}>
                <CardHeader bgColor='#edf1f7'>
                    <Heading size='lg' fontWeight={400}>Public / Private Key Pairs</Heading>
                </CardHeader>
                <CardBody className='space-y-4'>
                    <div className='text-black text-base'>Private Key</div>
                    <InputGroup size='md'>
                        <Input placeholder='' />
                        <InputRightAddon children='Random' bgColor='#727b84' textColor='white'/>
                    </InputGroup>

                    <div>Public Key</div>
                    <InputGroup size='md' disabled>
                        <Input disabled bgColor='#edf1f7' />
                    </InputGroup> 
                </CardBody>
            </Card>
        </div>
    </div>
  )
}

export default KeysComponent
