import React, { useState } from 'react'
import { Card, CardHeader, Heading, CardBody, Text, CardFooter, Button, InputGroup, InputRightAddon, Input  } from '@chakra-ui/react'


const Verify = () => {
    return (
    <>
        <CardBody className='space-y-4'>
            <div className='text-black text-base'>Message</div>
            <Input placeholder='' htmlSize={148} width='auto' height='100px' />

            <div>Public Key</div>
            <Input   />

            <div> Signature</div>
            <Input   />
            <Button width='1160px' bgColor='blue' textColor='white'>Sign</Button>
        </CardBody>
    </>
    )
}
const Sign = () => {
    return (
    <>
        <CardBody className='space-y-4'>
            <div className='text-black text-base'>Message</div>
            <Input placeholder='' htmlSize={148} width='auto' height='100px' />

            <div>Private Key</div>
            <Input  />
            <Button width='1160px' bgColor='blue' textColor='white'>Sign</Button>

            <div>Message Signature</div>
            <Input  />
        </CardBody>
    </>
    )
}

const SignatureComponent = () => {
    const [title, setTitle] = useState('Sign')

    const handleSign = () => {
        setTitle('Sign')
    }

    const handleVerify = () => {
        setTitle('Verify')
    }
  return (
    <div className="">
        <div className="pt-8 flex justify-center">
        <Card width={1200} height={600}>
            <CardHeader bgColor='#edf1f7' className='space-y-4'>
                <Heading size='lg' fontWeight={400}>
                    Signatures <br/>
                </Heading>
                <div className="space-x-4 ">
                    <span className='text-xl cursor-pointer hover:text-blue-500 ' onClick={handleSign}>Sign</span>
                    <span className='text-xl  cursor-pointer hover:text-blue-500' onClick={handleVerify}>Verify</span>
                </div>
            </CardHeader>
            {
                title === 'Sign' ? <Sign /> : <Verify />
            }
            
        </Card>
        </div>
    </div>
  )
}

export default SignatureComponent